# table_orders Test task for Syntech (Python)

How to install:
1. sudo apt-get install python3-pip
2. sudo pip3 install virtualenv
3. cd /path/to/project
4. virtualenv venv
5. git clone https://bakutake@bitbucket.org/bakutake/table_orders.git
6. source venv/bin/activate
7. cd 'table_orders'
8. pip install -r requirements.txt
9. python manage.py migrate
10. python manage.py createsuperuser

How to run:
1. cd /path/to/project
2. cd 'table_orders'
3. python manage.py runserver

How to test:
1. coverage run --source='.' manage.py test
2. coverage html
4. cd /path/to/project
5. !not command! open ../htmlcov/index.html

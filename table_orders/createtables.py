from table_orders.models import Table
import random


def create_table(left, top, width, height):
    table = Table(
        number_of_seats=random.randint(0, 5),
        shape=random.choice(Table.SHAPE_CHOISES)[0],
        coordinate_left=left,
        coordinate_top=top,
        width=width,
        height=height,
    )
    table.save()
    return table


def clear_tables():
    Table.objects.all().delete()


def _fill_hall(delete_current_tables=True):
    if delete_current_tables:
        clear_tables()

    for top in range(5, 100, 20):
        for left in range(5, 100, 20):
            create_table(left, top, 10, 10)


def fill_hall(apps, schema_editor):
    _fill_hall()

from django.db import models
from django.utils.translation import gettext_lazy as _
from django.dispatch import receiver
from django.db.models.signals import post_delete, post_save
from django.core.cache import cache
from django.core.exceptions import ValidationError
import datetime


class Table(models.Model):

    # consts
    RECTANGLE = 'rectangle'
    ELLIPSE = 'ellipse'
    SHAPE_CHOISES = (
        (RECTANGLE, _('rectangle')),
        (ELLIPSE, _('ellipse')),
    )

    # fields
    number = models.AutoField(
        verbose_name=_('number'),
        blank=False,
        null=False,
        primary_key=True
    )
    number_of_seats = models.PositiveIntegerField(
        verbose_name=_('number_of_seats'),
        null=False,
        blank=False,
        default=1
    )
    shape = models.CharField(
        verbose_name=_("shape"),
        blank=False,
        null=False,
        max_length=120,
        default=_(RECTANGLE),
        choices=SHAPE_CHOISES,
    )
    coordinate_left = models.PositiveSmallIntegerField(
        verbose_name=_('coordinate_left'),
        blank=False,
        null=False
    )
    coordinate_top = models.PositiveSmallIntegerField(
        verbose_name=_('coordinate_top'),
        blank=False,
        null=False
    )
    width = models.PositiveSmallIntegerField(
        verbose_name=_('width'),
        blank=False,
        null=False
    )
    height = models.PositiveSmallIntegerField(
        _('height'),
        blank=False,
        null=False
    )

    class Meta:
        verbose_name = _('table')
        verbose_name_plural = _('tables')
        ordering = ['-number']
        # constraints = [
        #     models.CheckConstraint(check=models.Q(coordinate_left__gte=0, coordinate_left__lte=100), name='coordinate_left check'),
        #     models.CheckConstraint(check=models.Q(coordinate_top__gte=0, coordinate_top__lte=100), name='coordinate_top check'),
        #     models.CheckConstraint(check=models.Q(width__gte=0, width__lte=100), name='width check'),
        #     models.CheckConstraint(check=models.Q(height__gte=0, height__lte=100), name='height check'),
        # ]

    def size(self):
        return (self.width, self.height)

    def coordinates(self):
        return (self.coordinate_left, self.coordinate_top)

    def __str__(self):
        return _('table') + ' ' + str(self.number)


@receiver(post_delete, sender=Table)
def table_post_delete_handler(sender, **kwargs):
    all_tables = Table.objects.all()
    cache.set('all_tables', all_tables) 


@receiver(post_save, sender=Table)
def table_post_save_handler(sender, **kwargs):
    all_tables = Table.objects.all()
    cache.set('all_tables', all_tables)


class OrderTable(models.Model):

    order_date = models.DateField(_('order_date'), default=datetime.datetime.today)
    tables = models.ManyToManyField(Table, help_text=_('tables'))
    first_name = models.CharField(_('first_name'), blank=False, null=False, max_length=100)
    last_name = models.CharField(_('last_name'), blank=False, null=False, max_length=100)
    email = models.EmailField(_('email'), null=False, blank=False)

    class Meta:
        # default_related_name = 'order_table'
        verbose_name = _('order')
        verbose_name_plural = _('orders')
        ordering = ['-order_date']
        indexes = [
            models.Index(fields=['order_date'], name='order_date_index'),
        ]

    def __str__(self):
        return "{order} {num_order} - {full_name}".format(
            order=_('order'),
            num_order=self.pk,
            full_name=self.full_name(),
        )

    def full_name(self):
        return str(self.first_name) + ' ' + str(self.last_name)

from django.contrib import admin
from table_orders.models import OrderTable, Table

# Register your models here.
class TableAdmin(admin.ModelAdmin):
    readonly_fields = ('number',)

admin.site.register(Table, TableAdmin)
admin.site.register(OrderTable)

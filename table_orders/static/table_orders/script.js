let tables = document.querySelectorAll('.hall .table:not(.table-taken)');
let setActive = function() {
  if (this.classList.contains('active')) {
    this.classList.remove('active');
  } else {
    this.classList.add('active');
  }
  checkVisibleForm();
};

for (let i = 0; i < tables.length; i++) {
  tables[i].addEventListener('click', setActive);
}

var checkVisibleForm = function() {
  let form = document.getElementById('orderTableForm');
  if (document.querySelector('.hall .table.active') !== null) {
    if (form.classList.contains('d-none')) {
      form.classList.remove('d-none');
    }
  } else {
    if (!form.classList.contains('d-none')) {
      form.classList.add('d-none');
    }
  }
}

// let orderDateInput = document.getElementById("orderDate");
// changeDate = function() {
//   window.location.href = "/order/" + orderDateInput.value.replace('-', '/').replace('-', '/');
//   window.location.replace("/order/" + orderDateInput.value.replace('-', '/').replace('-', '/'));
// }
// orderDateInput.addEventListener('change', changeDate);


$(document).ready(function() {

  let orderDateInput = document.getElementById("orderDate");
  changeDate = function() {
    window.location.href = "/order/" + orderDateInput.value.replace('-', '/').replace('-', '/');
  }
  orderDateInput.addEventListener('change', changeDate);

  checkVisibleForm();
  let sendData = function() {
    let form = document.getElementById('orderTableForm');
    // let formData = new FormData(form);
    let selected_tables = document.querySelectorAll('.hall .table.active');
    let selected_tables_ids = new Array();

    let tablesMultiSelect = document.createElement("select");
    tablesMultiSelect.setAttribute('multiple', '');
    tablesMultiSelect.setAttribute('name', 'tables');
    tablesMultiSelect.setAttribute('style', 'display:none;');

    for (let i = 0; i < selected_tables.length; i++) {
      let tableOption = document.createElement('option');
      tableOption.setAttribute('value', selected_tables[i].textContent);
      tableOption.setAttribute('selected', '');
      tablesMultiSelect.appendChild(tableOption);
    }
    form.appendChild(tablesMultiSelect);
  }
  let form = document.getElementById('orderTableForm');
  form.addEventListener('submit', sendData);
});

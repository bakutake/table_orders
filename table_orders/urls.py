from django.urls import path, re_path
from django.shortcuts import redirect, reverse
from table_orders import views


app_name = 'table_orders'
urlpatterns = [
    path('order/<int:year>/<int:month>/<int:day>/',
         views.table_order, name='table_order'),
    path('order/', views.table_order, name='table_order_today'),
    path('order/success/', views.success_order, name='success_order'),
    path('', views.index, name='index')
]

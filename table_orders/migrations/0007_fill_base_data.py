from django.db import migrations, models
from table_orders import createtables

class Migration(migrations.Migration):

    dependencies = [
        ('table_orders', '0006_auto_20190419_1932'),
    ]

    operations = [
        migrations.RunPython(createtables.fill_hall),
    ]

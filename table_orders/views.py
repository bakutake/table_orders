from django.shortcuts import render, HttpResponse, get_list_or_404, get_object_or_404, reverse, redirect
from django.views import generic
import datetime

from django.utils.translation import gettext_lazy as _
from table_orders.models import OrderTable, Table
from table_orders.forms import OrderTableForm
from django.core.cache import cache
from django.core.mail import send_mail


def table_order(request, year=datetime.date.today().year,
                month=datetime.date.today().month,
                day=datetime.date.today().day):

    current_date = datetime.date(year, month, day)

    all_tables = cache.get('all_tables')
    if all_tables is None:
        all_tables = Table.objects.all()
        cache.set('all_tables', all_tables)
    taken_tables = Table.objects.filter(ordertable__order_date=current_date).only('number')
    selected_tables = [int(num) for num in request.POST.getlist('tables')]

    form = OrderTableForm(current_date=current_date)
    if request.method == 'POST':
        form = OrderTableForm(request.POST, current_date=current_date)
        if current_date < datetime.date.today():
            form.add_error(None, 'passed_date')

        if form.is_valid():
            new_order = form.save(commit=False)
            new_order.order_date = current_date
            new_order.save()
            form.save_m2m()

            # send_mail(
            #     _('table_order'),
            #     # указать email, last_name, first_name, и номера столиков + карту местоположения, где их столики выделены?
            #     _('table_order_body_message').format(),
            #     'from@localhost',
            #     ['bamboo.babah@gmail.com'],
            # )
            return redirect('table_orders:success_order')

    context = {
        'all_tables': all_tables,
        'taken_tables': taken_tables,
        'selected_tables': selected_tables,
        'form': form,
        'navlink': 'table_orders:order_table',
        'date': current_date,
    }

    return render(request, 'table_orders/table_order.html', context=context)

def success_order(request):
    return render(request, 'table_orders/success_order.html')

def index(request):
    return redirect(reverse('table_orders:table_order_today'))

from django.contrib.auth.models import User, AnonymousUser
from django.shortcuts import reverse
from django.test import Client, TestCase, override_settings
from django.core.cache import cache
import unittest
import random
import datetime

from table_orders.models import OrderTable, Table

class TableOrderTest(TestCase):

    def setUp(self):
        # cache.clear()
        self.maxDiff = None
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def _create_random_table(self):
        table = Table(
            number_of_seats=random.randint(0, 5),
            shape=random.choice(Table.SHAPE_CHOISES)[0],
            coordinate_left=random.randint(0, 101),
            coordinate_top=random.randint(0, 101),
            width=random.randint(0, 101),
            height=random.randint(0, 101),
        )
        table.save()
        return table

    def test_update_cache_after_save(self):
        self._create_random_table()
        tables_before = Table.objects.all()
        tables_before_from_cache = cache.get('all_tables')
        count_tables_before_from_cache = tables_before_from_cache.count()
        list(tables_before)  # django querysets are lazy, query makes when we get's data

        self._create_random_table()
        tables_after = Table.objects.all()
        tables_after_from_cache = cache.get('all_tables')
        count_tables_after_from_cache = tables_after_from_cache.count()
        list(tables_before)  # django querysets are lazy, query makes when we get's data
        self.assertQuerysetEqual(tables_before, map(repr, tables_before_from_cache))
        self.assertQuerysetEqual(tables_after, map(repr, tables_after_from_cache))
        self.assertEqual(count_tables_before_from_cache + 1,
                         count_tables_after_from_cache)

    def test_update_cache_after_delete(self):
        for i in range(10):
            self._create_random_table()
        tables_before = Table.objects.all()
        tables_before_from_cache = cache.get('all_tables')
        count_tables_before_from_cache = tables_before_from_cache.count()
        list(tables_before)  # django querysets are lazy, query makes when we get's data

        Table.objects.last().delete()
        tables_after = Table.objects.all()
        tables_after_from_cache = cache.get('all_tables')
        count_tables_after_from_cache = tables_after_from_cache.count()
        list(tables_before)  # django querysets are lazy, query makes when we get's data
        self.assertQuerysetEqual(tables_before, map(repr, tables_before_from_cache))
        self.assertQuerysetEqual(tables_after, map(repr, tables_after_from_cache))
        self.assertEqual(count_tables_before_from_cache - 1,
                         count_tables_after_from_cache)

from django.contrib.auth.models import User, AnonymousUser
from django.shortcuts import reverse
from django.test import Client, TestCase, override_settings
from django.core.cache import cache
import unittest
import random
import datetime

from table_orders.models import OrderTable, Table


class IndexTestCase(TestCase):

    def setUp(self):
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def test_redirect_from_main(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/order/')
        self.assertRedirects(response, reverse('table_orders:table_order_today'))


class TableOrderTestCase(TestCase):

    def setUp(self):
        # cache.clear()
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )
        for i in range(25):
            self._create_random_table()

    def _create_random_table(self):
        table = Table(
            number_of_seats=random.randint(0, 5),
            shape=random.choice(Table.SHAPE_CHOISES)[0],
            coordinate_left=random.randint(0, 101),
            coordinate_top=random.randint(0, 101),
            width=random.randint(0, 101),
            height=random.randint(0, 101),
        )
        table.save()
        return table


    def test_order_tables_get(self):
        response = self.client.get('/order/')
        tables = Table.objects.all()
        taken_tables = Table.objects.filter(ordertable__order_date=datetime.date.today()).only('number')
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['all_tables'], map(repr, tables))
        self.assertQuerysetEqual(response.context['taken_tables'], map(repr, taken_tables))

    def test_order_tables_get_with_cleared_cache(self):
        cache.clear()
        response = self.client.get('/order/')
        tables = Table.objects.all()
        taken_tables = Table.objects.filter(ordertable__order_date=datetime.date.today()).only('number')
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['all_tables'], map(repr, tables))
        self.assertQuerysetEqual(response.context['taken_tables'], map(repr, taken_tables))

    def test_order_tables_get_with_cache(self):
        response = self.client.get('/order/')
        response = self.client.get('/order/')
        tables = Table.objects.all()
        taken_tables = Table.objects.filter(ordertable__order_date=datetime.date.today()).only('number')
        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context['all_tables'], map(repr, tables))
        self.assertQuerysetEqual(response.context['taken_tables'], map(repr, taken_tables))

    def test_order_tables_post_correct_data(self):
        today = datetime.date.today()
        data = {
            'first_name': 'examle_first_name',
            'last_name': 'examle_last_name',
            'email': 'email@examle.com',
            'tables': ['10', '15', '20'],
        }
        order_tables_before = OrderTable.objects.filter(order_date=today)
        order_tables_count_before = order_tables_before.count()

        response = self.client.post('/order/', data=data)

        order_tables_after = OrderTable.objects.filter(order_date=today)
        order_tables_count_after = order_tables_after.count()
        last_order = order_tables_after.order_by('order_date').last()
        ordered_tables = Table.objects.filter(number__in=[10, 15, 20])

        self.assertEqual(response.status_code, 302)
        self.assertEqual(order_tables_count_before + 1, order_tables_count_after)
        self.assertEqual(last_order.first_name, 'examle_first_name')
        self.assertEqual(last_order.last_name, 'examle_last_name')
        self.assertEqual(last_order.email, 'email@examle.com')
        self.assertQuerysetEqual(last_order.tables.all(), map(repr, ordered_tables))

    def test_order_tables_post_invalid_data(self):
        today = datetime.date.today()
        data = {
            'first_name': 'examle_first_name',
            'last_name': '',
            'email': 'email@examle.com',
            'tables': ['10', '15', '20'],
        }
        order_tables_before = OrderTable.objects.filter(order_date=today)
        order_tables_count_before = order_tables_before.count()

        response = self.client.post('/order/', data=data)

        order_tables_after = OrderTable.objects.filter(order_date=today)
        order_tables_count_after = order_tables_after.count()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(order_tables_count_before, order_tables_count_after)
        self.assertEqual(response.context['form']['first_name'].errors, [])
        self.assertGreater(len(response.context['form']['last_name'].errors), 0)
        self.assertFalse(response.context['form'].is_valid())

    def test_order_tables_post_yesterday(self):
        today = datetime.date.today()
        yesterday = today - datetime.timedelta(days=1)
        data = {
            'first_name': 'examle_first_name',
            'last_name': 'examle_last_name',
            'email': 'email@examle.com',
            'tables': ['10', '15', '20'],
        }
        order_tables_before = OrderTable.objects.filter(order_date=today)
        order_tables_count_before = order_tables_before.count()

        url = '/order/{year}/{month}/{day}/'.format(
            year=yesterday.year,
            month=yesterday.month,
            day=yesterday.day,
        )
        response = self.client.post(url, data=data)

        order_tables_after = OrderTable.objects.filter(order_date=today)
        order_tables_count_after = order_tables_after.count()

        self.assertEqual(response.status_code, 200)
        self.assertEqual(order_tables_count_before, order_tables_count_after)
        self.assertGreater(len(response.context['form'].errors), 0)
        self.assertFalse(response.context['form'].is_valid())

class SuccessOrderTestCase(TestCase):

    def setUp(self):
        from django.conf import settings
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def test_success_order_get(self):
        response = self.client.get('/order/success/')
        self.assertEqual(response.status_code, 200)

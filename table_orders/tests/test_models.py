from django.contrib.auth.models import User, AnonymousUser
from django.shortcuts import reverse
from django.test import Client, TestCase, override_settings
from django.core.cache import cache
from django.utils.translation import gettext_lazy as _

import unittest
import random
import datetime

from table_orders.models import OrderTable, Table

class TableTest(TestCase):

    def setUp(self):
        # cache.clear()
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def _create_random_table(self):
        table = Table(
            number_of_seats=random.randint(0, 5),
            shape=random.choice(Table.SHAPE_CHOISES)[0],
            coordinate_left=random.randint(0, 101),
            coordinate_top=random.randint(0, 101),
            width=random.randint(0, 101),
            height=random.randint(0, 101),
        )
        table.save()
        return table

    def test_table_creation(self):
        table = self._create_random_table()
        self.assertIsInstance(table, Table)

    def test_table_str(self):
        table = self._create_random_table()
        self.assertEqual(str(table), _('table') + ' ' + str(table.number))

    def test_table_size(self):
        table = self._create_random_table()
        self.assertEqual(table.size(),
                         (table.width, table.height))

    def test_table_coordinates(self):
        table = self._create_random_table()
        self.assertEqual(table.coordinates(),
                         (table.coordinate_left, table.coordinate_top))


class TableOrderTest(TestCase):

    def setUp(self):
        # cache.clear()
        self.client = Client()
        self.admin = User.objects.create_superuser(
            username='admin',
            email='admin@admin.com',
            password='password',
        )
        self.anonym = AnonymousUser()
        self.user = User.objects.create_user(
            username='user',
            email='mail@mail.com',
            password='password',
        )

    def _create_random_table(self):
        table = Table(
            number_of_seats=random.randint(0, 5),
            shape=random.choice(Table.SHAPE_CHOISES)[0],
            coordinate_left=random.randint(0, 101),
            coordinate_top=random.randint(0, 101),
            width=random.randint(0, 101),
            height=random.randint(0, 101),
        )
        table.save()
        return table

    def _make_order(self, tables):
        order = OrderTable(
            order_date=datetime.date.today(),
            first_name='first_name',
            last_name='last_name',
            email='examle@mail.com',
        )
        order.save()
        for table in tables:
            order.tables.add(table)
        order.save()
        return order

    def test_order_table_creation(self):
        table = self._create_random_table()
        order = self._make_order([table])
        self.assertIsInstance(order, OrderTable)

    def test_order_table_str(self):
        table = self._create_random_table()
        order = self._make_order([table])
        self.assertEqual(str(order), "{order} {num_order} - {full_name}".format(
            order=_('order'),
            num_order=order.pk,
            full_name=order.full_name(),
        ))

    def test_order_table_full_name(self):
        table = self._create_random_table()
        order = self._make_order([table])
        self.assertEqual(order.full_name(),
                         str(order.first_name) + ' ' + str(order.last_name))

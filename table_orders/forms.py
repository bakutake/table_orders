from django.forms import ModelForm
from django.forms import ModelForm, Textarea
from django.core.cache import cache
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

from table_orders.models import Table, OrderTable


class OrderTableForm(ModelForm):
    class Meta:
        model = OrderTable
        exclude = ('order_date', )

    def __init__(self, *args, **kwargs):
        self.current_date = kwargs.get('current_date')
        if 'current_date' in kwargs:
            del kwargs['current_date']
        super().__init__(*args, **kwargs)

        self.fields['first_name'].widget.attrs.update({
            'type': 'text',
            'class': 'form-control',
        })
        self.fields['last_name'].widget.attrs.update({
            'type': 'text',
            'class': 'form-control',
        })
        self.fields['email'].widget.attrs.update({
            'type': 'text',
            'class': 'form-control',
        })
        self.fields['tables'].widget.attrs.update({
            'class': 'form-control',
        })

    def clean(self):
        data = self.cleaned_data

        all_tables = Table.objects.only('number')
        taken_tables = Table.objects.filter(ordertable__order_date=self.current_date).only('number')
        selected_tables = self.cleaned_data.get('tables')

        if not selected_tables:
            self.add_error(None, _('tables_is_not_selected'))
            self.add_error('tables', _('tables_is_not_selected'))

        if all_tables and taken_tables and selected_tables:
            for table in selected_tables:
                if table in taken_tables:
                    self.add_error(None, _('table_is_taken'))
                    self.add_error('tables', _('table_is_taken'))

                if table not in all_tables:
                    self.add_error(None, _('table_not_in_list'))
                    self.add_error('tables', _('table_not_in_list'))

        if self.errors:
            self._add_validation_classes('is-invalid', 'is-valid')
        return data


    def _add_validation_classes(self, no_valid_css_class='',
                                valid_css_class=''):

        for field in self.fields:
            # add 'class' attribute if it's not exists and
            # add space if it's not empty
            if self.fields[field].widget.attrs.setdefault('class', '') != '':
                self.fields[field].widget.attrs['class'] += ' '

            # add validation classes
            if field not in self.cleaned_data:
                self.fields[field].widget.attrs['class'] += str(no_valid_css_class)
            else:
                self.fields[field].widget.attrs['class'] += str(valid_css_class)
